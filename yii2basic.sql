-- phpMyAdmin SQL Dump
-- version 4.0.4.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas wygenerowania: 15 Cze 2017, 20:11
-- Wersja serwera: 5.6.13
-- Wersja PHP: 5.4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `yii2basic`
--
CREATE DATABASE IF NOT EXISTS `yii2basic` DEFAULT CHARACTER SET utf8 COLLATE utf8_polish_ci;
USE `yii2basic`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `actor`
--

CREATE TABLE IF NOT EXISTS `actor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `date_of_death` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=5 ;

--
-- Zrzut danych tabeli `actor`
--

INSERT INTO `actor` (`id`, `name`, `date_of_birth`, `date_of_death`) VALUES
(1, 'tatar', '2017-06-01', NULL),
(3, 'tatar tatar', '2011-11-11', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `actor_to_edit`
--

CREATE TABLE IF NOT EXISTS `actor_to_edit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `actor_id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `date_of_death` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `actor_id` (`actor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=4 ;

--
-- Zrzut danych tabeli `actor_to_edit`
--

INSERT INTO `actor_to_edit` (`id`, `actor_id`, `name`, `date_of_birth`, `date_of_death`) VALUES
(3, 1, 'tatar', '2017-06-01', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `actor_to_remove`
--

CREATE TABLE IF NOT EXISTS `actor_to_remove` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `actor_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `actor_id` (`actor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `movie_id` int(10) unsigned NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `text` text COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`movie_id`),
  KEY `movie_id_2` (`movie_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=2 ;

--
-- Zrzut danych tabeli `comment`
--

INSERT INTO `comment` (`id`, `user_id`, `movie_id`, `date`, `text`) VALUES
(1, 1, 11, '2017-06-14 21:53:05', 'sdadsada');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `id` int(1) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=2 ;

--
-- Zrzut danych tabeli `country`
--

INSERT INTO `country` (`id`, `name`) VALUES
(1, 'Polska');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `director`
--

CREATE TABLE IF NOT EXISTS `director` (
  `actor_id` int(10) unsigned NOT NULL,
  `movie_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`actor_id`,`movie_id`),
  KEY `movie_id` (`movie_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `director`
--

INSERT INTO `director` (`actor_id`, `movie_id`) VALUES
(1, 8),
(1, 9),
(3, 10),
(1, 11);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `genre`
--

CREATE TABLE IF NOT EXISTS `genre` (
  `id` int(1) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=6 ;

--
-- Zrzut danych tabeli `genre`
--

INSERT INTO `genre` (`id`, `name`) VALUES
(4, ' Dramat'),
(5, 'Dramat'),
(3, 'Kryminał');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `movie`
--

CREATE TABLE IF NOT EXISTS `movie` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `budget` varchar(20) COLLATE utf8_polish_ci NOT NULL DEFAULT 'Brak',
  `poster` varchar(260) COLLATE utf8_polish_ci NOT NULL,
  `description` text COLLATE utf8_polish_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `year` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=12 ;

--
-- Zrzut danych tabeli `movie`
--

INSERT INTO `movie` (`id`, `title`, `budget`, `poster`, `description`, `user_id`, `year`) VALUES
(8, 'Mój Film', '2 zł', 'http://www.sa-wa-ro.com/SIDEPROJECTS-New/AAAA-Folder/aaaa-logo.gif', 'Bardzo fajny film', 1, 0),
(9, 'Film zajebisty', '15zł', 'http://ptl.katowice.pl/wp-content/gallery/plakaty/plakat-autyzm2012.jpg', 'dadasdas', 1, 0),
(10, 'Kolejny film', '15', 'http://www.galeriaplakatu.com.pl/media/products/129d41085240663ebbcdfc8856aba498/images/thumbnail/big_5169.jpg?lm=1471130640', 'dfadsa dsa dsa', 1, 0),
(11, 'I jeszcze jeden znakomity film', '150', 'https://www.marr.pl/multimedia/0002/5086/plakat-ekspert.jpg', 'dasd asdas dsa', 1, 1993);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `movie_country`
--

CREATE TABLE IF NOT EXISTS `movie_country` (
  `country_id` int(1) unsigned NOT NULL,
  `movie_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`country_id`,`movie_id`),
  KEY `movie_id` (`movie_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `movie_country`
--

INSERT INTO `movie_country` (`country_id`, `movie_id`) VALUES
(1, 8),
(1, 9),
(1, 10),
(1, 11);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `movie_genre`
--

CREATE TABLE IF NOT EXISTS `movie_genre` (
  `genre_id` int(1) unsigned NOT NULL,
  `movie_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`movie_id`,`genre_id`),
  KEY `genre_id` (`genre_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `movie_genre`
--

INSERT INTO `movie_genre` (`genre_id`, `movie_id`) VALUES
(3, 8),
(4, 8),
(5, 9),
(5, 10),
(5, 11);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `actor_id` int(10) unsigned NOT NULL,
  `movie_id` int(10) unsigned NOT NULL,
  `name` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `actor_id` (`actor_id`),
  KEY `movie_id` (`movie_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=6 ;

--
-- Zrzut danych tabeli `role`
--

INSERT INTO `role` (`id`, `actor_id`, `movie_id`, `name`) VALUES
(2, 3, 8, 'Koń'),
(3, 3, 9, 'Jazda'),
(4, 1, 10, 'lol'),
(5, 3, 11, 'mysz');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `unsanctioned_user`
--

CREATE TABLE IF NOT EXISTS `unsanctioned_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) COLLATE utf8_polish_ci NOT NULL,
  `password` varchar(20) COLLATE utf8_polish_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=13 ;

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `status`, `type`) VALUES
(1, 'admin', 'admin', 1, 1),
(2, 'demo', 'demo', 1, 0),
(11, 'tatar1', 'tatar123', 1, 0),
(12, 'tatar', 'tatar1', 1, 0);

--
-- Wyzwalacze `user`
--
DROP TRIGGER IF EXISTS `ACTIVATE_USER`;
DELIMITER //
CREATE TRIGGER `ACTIVATE_USER` AFTER UPDATE ON `user`
 FOR EACH ROW IF(OLD.`status` = 0 AND NEW.`status` = 1)  THEN
	DELETE FROM unsanctioned_user WHERE user_id = NEW.id;
END IF
//
DELIMITER ;
DROP TRIGGER IF EXISTS `UNSANCTIONED_USER`;
DELIMITER //
CREATE TRIGGER `UNSANCTIONED_USER` AFTER INSERT ON `user`
 FOR EACH ROW INSERT INTO unsanctioned_user VALUE (DEFAULT, NEW.id)
//
DELIMITER ;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `actor_to_edit`
--
ALTER TABLE `actor_to_edit`
  ADD CONSTRAINT `actor_to_edit_ibfk_1` FOREIGN KEY (`actor_id`) REFERENCES `actor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `actor_to_remove`
--
ALTER TABLE `actor_to_remove`
  ADD CONSTRAINT `actor_to_remove_ibfk_1` FOREIGN KEY (`actor_id`) REFERENCES `actor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `movie`
--
ALTER TABLE `movie`
  ADD CONSTRAINT `movie_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `unsanctioned_user`
--
ALTER TABLE `unsanctioned_user`
  ADD CONSTRAINT `unsanctioned_user_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
