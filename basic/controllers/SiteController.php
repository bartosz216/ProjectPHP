<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SignupForm;
use app\models\AdminPanelForm;
use app\models\AddMovieForm;
use app\models\AddActorForm;
use app\models\IndexForm;
use yii\data\ActiveDataProvider;
use app\models\Movie;
use app\models\Actor;
use app\models\MovieForm;
use app\models\ActorForm;
use app\models\EditActorForm;

class SiteController extends Controller {
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [ 
                'access' => [ 
                        'class' => AccessControl::className (),
                        'only' => [ 
                                'logout',
                                'signup' 
                        ],
                        'rules' => [ 
                                [ 
                                        'actions' => [ 
                                                'signup' 
                                        ],
                                        'allow' => true,
                                        'roles' => [ 
                                                '?' 
                                        ] 
                                ],
                                [ 
                                        'actions' => [ 
                                                'logout' 
                                        ],
                                        'allow' => true,
                                        'roles' => [ 
                                                '@' 
                                        ] 
                                ] 
                        ] 
                ],
                'verbs' => [ 
                        'class' => VerbFilter::className (),
                        'actions' => [ 
                                'logout' => [ 
                                        'post' 
                                ] 
                        ] 
                ] 
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function actions() {
        return [ 
                'error' => [ 
                        'class' => 'yii\web\ErrorAction' 
                ],
                'captcha' => [ 
                        'class' => 'yii\captcha\CaptchaAction',
                        'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null 
                ] 
        ];
    }
    
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        $model = new IndexForm ();
        
        if ($model->load ( Yii::$app->request->post () )) {
            return $this->redirect ( $model->search () );
        }
        
        return $this->render ( 'index', [ 
                'model' => $model 
        ] );
    }
    
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (! Yii::$app->user->isGuest) {
            return $this->goHome ();
        }
        
        $model = new LoginForm ();
        if ($model->load ( Yii::$app->request->post () ) && $model->login ()) {
            return $this->goBack ();
        }
        return $this->render ( 'login', [ 
                'model' => $model 
        ] );
    }
    
    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout ();
        
        return $this->goHome ();
    }
    
    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm ();
        if ($model->load ( Yii::$app->request->post () ) && $model->contact ( Yii::$app->params ['adminEmail'] )) {
            Yii::$app->session->setFlash ( 'contactFormSubmitted' );
            
            return $this->refresh ();
        }
        return $this->render ( 'contact', [ 
                'model' => $model 
        ] );
    }
    
    /**
     * Singup action.
     *
     * @return string
     */
    public function actionSignup() {
        $model = new SignupForm ();
        if ($model->load ( Yii::$app->request->post () )) {
            if ($user = $model->signup ()) {
                if (Yii::$app->getUser ()->login ( $user )) {
                    return $this->goHome ();
                }
            }
        }
        
        return $this->render ( 'signup', [ 
                'model' => $model 
        ] );
    }
    
    /**
     * Adminpanel action
     */
    public function actionAdminpanel() {
        $model = new AdminPanelForm ();
        
        if ($model->load ( Yii::$app->request->post () )) {
            $model->accept ();
            $this->refresh ();
        }
        
        return $this->render ( 'adminpanel', [ 
                'model' => $model 
        ] );
    }
    
    /**
     * Add movie action
     */
    public function actionAddmovie() {
        $model = new AddMovieForm ();
        
        if ($model->load ( Yii::$app->request->post () ) && $movie = $model->addMovie ()) {
            $this->refresh ();
        }
        
        return $this->render ( 'addmovie', [ 
                'model' => $model 
        ] );
    }
    
    /**
     * Add actor action
     */
    public function actionAddactor() {
        $model = new AddActorForm ();
        
        if ($model->load ( Yii::$app->request->post () ) && $actor = $model->addactor ()) {
            $this->refresh ();
        }
        
        return $this->render ( 'addactor', [ 
                'model' => $model 
        ] );
    }
    
    /**
     *
     * @return string
     */
    public function actionMovies() {
        $ids = Yii::$app->request->get ( 'ids' );
        
        if (empty ( $ids )) {
            $dataprovider = new ActiveDataProvider ( [ 
                    'query' => Movie::find ()->where(['status' => Movie::STATUS_ACTIVE]),
                    'pagination' => [ 
                            'pageSize' => 20 
                    ] 
            ] );
        } else {
            $dataprovider = new ActiveDataProvider ( [ 
                    'query' => Movie::find ()->filterWhere ( [ 
                            'in',
                            'id',
                            $ids 
                    ] ),
                    'pagination' => [ 
                            'pageSize' => 20 
                    ] 
            ] );
        }
        
        return $this->render ( 'movies', [ 
                'dataProvider' => $dataprovider 
        ] );
    }
    
    /**
     *
     * @return string
     */
    public function actionActors() {
        $ids = Yii::$app->request->get ( 'ids' );
        if (empty ( $ids )) {
            $dataprovider = new ActiveDataProvider ( [ 
                    'query' => Actor::find (),
                    'pagination' => [ 
                            'pageSize' => 20 
                    ] 
            ] );
        } else {
            $dataprovider = new ActiveDataProvider ( [ 
                    'query' => Actor::find ()->filterWhere ( [ 
                            'in',
                            'id',
                            $ids 
                    ] ),
                    'pagination' => [ 
                            'pageSize' => 20 
                    ] 
            ] );
        }
        
        return $this->render ( 'actors', [ 
                'dataProvider' => $dataprovider 
        ] );
    }
    
    /**
     *
     * @return string
     */
    public function actionMovie() {
        $model = new MovieForm ();
        if( $model->load ( Yii::$app->request->post () )){
	        if (isset ( $_POST ['delete-button'] )) {
	            if ($movie_to_remove = $model->remove ()) {
	                \Yii::$app->session->setFlash ( 'success', 'Twoje zgłoszenie zostało wysłane.' );
	            }
	        } else if (isset ( $_POST ['edit-button'] )) {
	            \Yii::trace ( 'edit button clicked' );
	            /*
	             * return $this->redirect ( [
	             * 'editactor',
	             * 'id' => $model->actor->id
	             * ] );
	             */
	        } else if (isset ( $_POST ['rate-button'] ) ) {
	            if ($makr = $model->addMark ()) {
	                \Yii::$app->session->setFlash ( 'success', 'Ocena została dodana.' );
	            }
	        } else if (isset ( $_POST ['comment-button'] )) {
	            if ($comment = $model->addComment ()) {
	                $this->refresh ();
	            }
	        }   
        }
        
        return $this->render ( 'movie', [ 
                'model' => $model 
        ] );
    }
    
    /**
     *
     * @return string
     */
    public function actionActor() {
        $model = new ActorForm ();
        $model->actor = Actor::findById ( Yii::$app->request->get ( 'id' ) );
        
        if (isset ( $_POST ['delete-button'] )) {
            if($model->remove()){
                \Yii::$app->session->setFlash ( 'success', 'Twoje zgłoszenie zostało wysłane' );
            }
        } else if (isset ( $_POST ['edit-button'] )) {
            return $this->redirect ( [ 
                    'editactor',
                    'id' => $model->actor->id 
            ] );
        }
        return $this->render ( 'actor', [ 
                'model' => $model 
        ] );
    }
    
    /**
     *
     * @return string
     */
    public function actionEditactor() {
        $model = new EditActorForm ();
        
        if ($model->load ( Yii::$app->request->post () ) && $editActor = $model->editActor ()) {
            \Yii::$app->session->setFlash ( 'success', 'Twoje zgłoszenie zostało wysłane' );
            return $this->redirect ( [ 
                    'actor',
                    'id' => $model->id 
            ] );
        }
        
        return $this->render ( 'editactor', [ 
                'model' => $model 
        ] );
    }
}
