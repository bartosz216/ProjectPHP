<?php

namespace app\models;

use yii\base\Model;

/**
 * AdminPanelForm is the model behind the contact form.
 */
class AddActorForm extends Model {
	public $name;
	public $date_of_birth;
	public $date_of_death;
	/**
	 *
	 * @return array the validation rules.
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'name',
								'date_of_birth' 
						],
						'required' 
				],
				[ 
						'name',
						'string',
						'max' => 100 
				] 
		];
	}
	
	/**
	 *
	 * @return array customized attribute labels
	 */
	public function attributeLabels() {
		return [ 
				'name' => 'Imię i nazwisko',
				'date_of_birth' => 'Data urodzenia',
				'date_of_death' => 'Data śmierci'
		];
	}
	
	/**
	 * Add actor.
	 *
	 * @return Actor|null the saved model or null if saving fails
	 */
	public function addactor() {
		if (! $this->validate ()) {
			return null;
		}
		
		$actor = new Actor();
		$actor->name = $this->name;
		$actor->date_of_birth = $this->date_of_birth;
		$actor->date_of_death = $this->date_of_death;
		
		return $actor->save ()? $actor : null;
	}
}
