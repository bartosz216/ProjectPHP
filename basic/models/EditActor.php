<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Actor model
 *
 * @property integer $id
 * @property integer $actor_id
 * @property string $name
 * @property date $date_of_birth
 * @property date $date_of_death
 */
class EditActor extends ActiveRecord {
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getActor() {
		return $this->hasOne ( Actor::className (), [
				'id' => 'actor_id'
		] );
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%actor_to_edit}}';
	}
	
	/**
	 * Finds actor by id
	 *
	 * @param string $id
	 * @return static|null
	 */
	public static function findById($id) {
		return static::findOne ( [
				'id' => $id
		] );
	}
	
	/**
	 * Finds actor to remove by actor_id
	 *
	 * @param integer $actor_id
	 * @return static|null
	 */
	public static function findByActorId($actor_id) {
		return static::findOne ( [ 'actor_id' => $actor_id] );
	}
}
