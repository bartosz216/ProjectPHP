<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Comment model
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $movie_id
 * @property timestamp $date
 * @property string $text
 */
class Comment extends ActiveRecord {
	
	public function getUser()
	{
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}
	
	public function getMovie()
	{
		return $this->hasOne(Movie::className(), ['id' => 'movie_id']);
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%comment}}';
	}
	
	/**
	 * Finds comment by id
	 *
	 * @param string $id
	 * @return static|null
	 */
	public static function findById($id) {
		return static::findOne ( [ 'id' => $id ] );
	}
	
	/**
	 * Finds comment by $movie_id
	 *
	 * @param string $id
	 * @return static|null
	 */
	public static function findByMovieId($id){
		return static::findAll( ['movie_id' => $id ] );
	}
	
	/**
	 * Finds comment by $movie_id and user_id
	 *
	 * @param string $movie_id
	 * @param string $user_id
	 * @return static|null
	 */
	public static function findByMovieAdnUserId($movie_id, $user_id){
	    return static::findAll( ['movie_id' => $movie_id, 'user_id' => $user_id ] );
	}
}