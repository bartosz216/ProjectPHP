<?php

namespace app\models;

use yii\base\Model;

/**
 * EditActorForm is the model behind the contact form.
 */
class EditActorForm extends Model {
	public $id;
	public $name;
	public $date_of_birth;
	public $date_of_death;
	
	/**
	 *
	 * @return array the validation rules.
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'name',
								'date_of_birth' 
						],
						'required' 
				],
				[ 
						'name',
						'string',
						'max' => 100 
				] 
		];
	}
	
	/**
	 *
	 * @return array customized attribute labels
	 */
	public function attributeLabels() {
		return [ 
				'name' => 'Imię i nazwisko',
				'date_of_birth' => 'Data urodzenia',
				'date_of_death' => 'Data śmierci' 
		];
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \yii\base\Object::init()
	 */
	public function init() {
		$actor = Actor::findById ( \Yii::$app->request->get ( 'id' ) );
		$this->id = $actor->id;
		$this->name = $actor->name;
		$this->date_of_birth = $actor->date_of_birth;
		$this->date_of_death = $actor->date_of_death;
	}
	
	/**
	 * 
	 * @return \app\models\EditActor
	 */
	public function editActor() {
		$editActor = new EditActor ();
		$editActor->actor_id = $this->id;
		$editActor->name = $this->name;
		$editActor->date_of_birth = $this->date_of_birth;
		$editActor->date_of_death = $this->date_of_death;
		$editActor->save ();
		
		return $editActor;
	}
}
