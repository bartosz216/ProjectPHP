<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Genre model
 *
 * @property integer $id
 * @property string $name
 */
class Genre extends ActiveRecord {
	
	public function getMovies()
	{
		return $this->hasMany(Movie::className(), ['id' => 'movie_id'])
			->viaTable('movie_genre', ['genre_id' => 'id']);
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%genre}}';
	}
	
	/**
	 * Finds genre by id
	 *
	 * @param string $id
	 * @return static|null
	 */
	public static function findById($id) {
		return static::findOne ( [ 'id' => $id ] );
	}
	
	/**
	 * Finds genre by $name
	 *
	 * @param string $name
	 * @return static|null
	 */
	public static function findByName($name){
		return static::findOne( ['name' => $name ] );
	}
}