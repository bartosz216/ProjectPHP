<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Country model
 *
 * @property integer $id
 * @property string $name
 */
class Country extends ActiveRecord {
	
	public function getMovies()
	{
		return $this->hasMany(Movie::className(), ['id' => 'movie_id'])
			->viaTable('movie_country', ['country_id' => 'id']);
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%country}}';
	}
	
	/**
	 * Finds comment by id
	 *
	 * @param string $id
	 * @return static|null
	 */
	public static function findById($id) {
		return static::findOne ( [ 'id' => $id ] );
	}
	
	/**
	 * Finds comment by $name
	 *
	 * @param string $name
	 * @return static|null
	 */
	public static function findByName($name){
		return static::findOne( ['name' => $name ] );
	}
}