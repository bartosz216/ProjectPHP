<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Movie model
 *
 * @property integer $id
 * @property string $title
 * @property string $budget
 * @property string $poster
 * @property string $description
 * @property integer $year
 * @property integer $user_id
 * @property integer $status
 */
class Movie extends ActiveRecord{
	const STATUS_INACTIVE = 0;
	const STATUS_ACTIVE = 1;
	
	public function getComments()
	{
		return $this->hasMany(Comment::className(), ['movie_id' => 'id']);
	}
	
	public function getCounties()
	{
		return $this->hasMany(Country::className(), ['id' => 'country_id'])
			->viaTable('movie_country', ['movie_id' => 'id']);
	}
	
	public function getActorsA()
	{
		return $this->hasMany(Actor::className(), ['id' => 'actor_id'])
			->viaTable('role', ['movie_id' => 'id'], null, ['name']);
	}
	
	public function getActorsD()
	{
		return $this->hasMany(Actor::className(), ['id' => 'actor_id'])
			->viaTable('director', ['movie_id' => 'id']);
	}
	
	public function getGenres()
	{
		return $this->hasMany(Genre::className(), ['id' => 'genre_id'])
			->viaTable('movie_genre', ['genre_id' => 'id']);
	}
	
	public function getUser(){
	    return $this->hasOne(User::className(), ['id' => 'user_id']);
	}
	
	public function getMarks(){
	    return $this->hasMany(Mark::className(), ['movie_id' => 'id']);
	}
	
	public function getMovieToRemove(){
	    return $this->hasOne(MovieToRemove::className(), ['movie_id' => 'id']);
	}
	
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
				[
						'status',
						'default',
						'value' => self::STATUS_INACTIVE
				],
				[
						'status',
						'in',
						'range' => [
								self::STATUS_INACTIVE,
								self::STATUS_ACTIVE
						]
				],
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%movie}}';
	}
	
	/**
	 * Finds movie by id
	 *
	 * @param string $id
	 * @return static|null
	 */
	public static function findById($id) {
		return static::findOne ( [ 'id' => $id ] );
	}
	
	
	/**
	 * Finds movie by title
	 *
	 * @param string $title
	 * @return static|null
	 */
	public static function findByTitle($title) {
		return static::findAll ( [ 'title' => $title] );
	}
}
