<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Actor model
 *
 * @property integer $id
 * @property integer $actor_id
 */
class ActorToRemove extends ActiveRecord {
	/**
	 * 
	 * @return \yii\db\ActiveQuery
	 */
	public function getActor() {
		return $this->hasOne ( Actor::className (), [ 
				'id' => 'actor_id' 
		] );
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%actor_to_remove}}';
	}
	
	/**
	 * Finds actor to remove by id
	 *
	 * @param string $id        	
	 * @return static|null
	 */
	public static function findById($id) {
		return static::findOne ( [ 
				'id' => $id 
		] );
	}
	
	/**
	 * Finds actor to remove by actor_id
	 *
	 * @param integer $actor_id        	
	 * @return static|null
	 */
	public static function findByActorId($actor_id) {
		return static::findOne ( [ 
				'actor_id' => $actor_id 
		] );
	}
}
