<?php

namespace app\models;

use Yii;
use Exception;
use yii\base\Model;

/**
 * AdminPanelForm is the model behind the contact form.
 */
class AddMovieForm extends Model {
	public $title;
	public $directors;
	public $genres;
	public $actors;
	public $countries;
	public $budget;
	public $poster;
	public $description;
	public $year;
	public $user_id;
	private $directorsList;
	private $actorsList;
	
	/**
	 *
	 * @return array the validation rules.
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'title',
								'directors',
								'genres',
								'actors',
								'countries',
								'budget',
								'poster',
								'description',
								'year'
						],
						'required' 
				],
		        [
		                'actors',
		                'validateActors'
		        ],
		        [
		                'directors',
		                'validateDirectors'
		        ],
		        [
		                'poster',
		                'validatePoster'
		        ]
		];
	}
	
	/**
	 *
	 * @return array customized attribute labels
	 */
	public function attributeLabels() {
		return [ 
				'title' => 'Tytuł‚',
				'directors' => 'Reżyser',
				'genres' => 'Rodzaj(e) filmu',
				'actors' => 'Aktorzy i role',
				'countries' => 'Kraj(e) pochodzenia',
				'budget' => 'Budżet',
				'poster' => 'Plakat',
				'description' => 'Opis',
				'year' => 'Rok produkcji'
		];
	}
	
	/**
	 */
	public function addMovie() {
		if (! $this->validate ()) {
			return null;
		}

		$transaction = Yii::$app->db->beginTransaction ();
		
		try {
			$movie = new Movie ();
			$movie->title = $this->title;
			$movie->budget = $this->budget;
			$movie->poster = $this->poster;
			$movie->description = $this->description;
			$movie->year = $this->year;
			$movie->user_id = Yii::$app->user->id;
			$movie->save ();
			
			foreach ( explode ( ',', $this->genres ) as $genreName ) {
				$genre = Genre::findByName ( $genreName );
				if ($genre === null) {
					$genre = new Genre ();
					$genre->name = $genreName;
					$genre->save ();
				}
				$movieGenre = new MovieGenre ();
				$movieGenre->movie_id = $movie->id;
				$movieGenre->genre_id = $genre->id;
				$movieGenre->save ();
			}
			
			foreach ( $this->directorsList as $director ) {
			    $d = new Director ();
			    $d->actor_id = $director->id;
			    $d->movie_id = $movie->id;
			    $d->save ();
			}
			
			foreach ( $this->actorsList as $actorRole ) {
			    $role = new Role ();
			    $role->name = $actorRole['role'];
			    $role->actor_id = $actorRole['actor']->id;
			    $role->movie_id = $movie->id;
			    $role->save ();
			}
			
			foreach ( explode ( ',', $this->countries ) as $countryName ) {
				$county = Country::findByName ( $countryName );
				
				if ($county === null) {
					$county = new Country ();
					$county->name = $countryName;
					$county->save ();
				}
				$movieCountry = new MovieCountry ();
				$movieCountry->country_id = $county->id;
				$movieCountry->movie_id = $movie->id;
				$movieCountry->save();
			}
			
			$transaction->commit ();
		} catch ( Exception $e ) {
			echo $e;
			$transaction->rollBack ();
		}
		
		return $movie;
	}
	
	/**
	 *
	 * @param string $attribute
	 * @param array $params
	 */
	public function validateActors($attribute, $params) {
	    if (! $this->hasErrors ()) {
	        $this->actors = trim ( preg_replace ( '/\s+/', ' ', $this->actors ) );
	        
	        $this->actorsList = array ();
	        foreach ( explode ( ',', $this->actors ) as $actorRole ) {
	            $actorName = explode ( ':', $actorRole );
	            $actor = Actor::findByName ( $actorName [0] );
	            if ($actor === null) {
	                $this->addError ( $attribute, "Aktor nie istnieje." );
	                break;
	            } else {
	                $this->actorsList [] = [
	                        'actor' => $actor,
	                        'role' => $actorName [1]
	                ];
	            }
	        }
	    }
	}
	
	/**
	 *
	 * @param string $attribute
	 * @param array $params
	 */
	public function validateDirectors($attribute, $params) {
	    if (! $this->hasErrors ()) {
	        $this->directors = trim ( preg_replace ( '/\s+/', ' ', $this->directors ) );
	        
	        $this->directorsList = array ();
	        foreach ( explode ( ',', $this->directors ) as $directorName ) {
	            $director = Actor::findByName ( $directorName );
	            if ($director === null) {
	                $this->addError ( $attribute, "Reżyser nie istnieje." );
	                break;
	            } else {
	                $this->directorsList [] = $director;
	            }
	        }
	    }
	}
	
	/**
	 *
	 * @param string $attribute
	 * @param array $params
	 */
	public function validatePoster($attribute, $params) {
	    if (! $this->hasErrors ()) {
	        //TODO walidacja plakatu, sprawdzić czy taki obrazek istnieje
	    }
	}
}
