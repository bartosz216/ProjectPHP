<?php

namespace app\models;

use yii\base\Model;
use app\models\UnsanctionedUser;
use app\models\User;

/**
 * AdminPanelForm is the model behind the contact form.
 */
class AdminPanelForm extends Model {
    public $reportType;
    public $reports;
    
    /**
     *
     * @return array the validation rules.
     */
    public function rules() {
        return [ 
                [ 
                        [ 
                                'reportType',
                                'reports' 
                        ],
                        'required' 
                ] 
        ];
    }
    
    /**
     *
     * @return array customized attribute labels
     */
    public function attributeLabels() {
        return [ 
                'reportType' => 'Typ zgłoszenia',
                'reports' => 'Wybierz zgłoszenie' 
        ];
    }
    public function getReportTypes() {
        return [ 
        		'Filmy do akceptacji',
                'Uzytkownicy niezaakceptowani',
                'Aktorzy do usunięcia',
                'Edytowani aktorzy',
                'Filmy do usunięcia' 
        ];
    }
    
    private function getMovieToActivate(){
    	$movies = [];
    	foreach(Movie::find()->where( ['status' => Movie::STATUS_INACTIVE] )->all() as $movie)
    	{
    		$movies[$movie->id] = $movie->title;
    	}
    	
    	return $movies;
    }
    
    /**
     *
     * @return string
     */
    public function getReport() {
    	return $this->getMovieToActivate();
        //return UnsanctionedUser::findAllUnsactionedUsers ();
    }
    
    /**
     */
    public function accept() {
        foreach ( $this->reports as $selectedMovie) {
        	$movie = Movie::find()->where([ 'id' => $selectedMovie, 'status' => Movie::STATUS_INACTIVE])->one();
            $movie->status = Movie::STATUS_ACTIVE;
            $movie->update ();
        }
    }
}
