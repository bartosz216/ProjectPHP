<?php

namespace app\models;

use yii\base\Model;

/**
 * AdminPanelForm is the model behind the contact form.
 */
class IndexForm extends Model {
    public $searchText;
    public $searchType;
    const MOVIE = 0;
    const ACTOR = 1;
    
    /**
     *
     * @return array the validation rules.
     */
    public function rules() {
        return [ 
                [ 
                        [ 
                                'searchText',
                                'searchType' 
                        ],
                        'required' 
                ] 
        ];
    }
    
    /**
     *
     * @return array customized attribute labels
     */
    public function attributeLabels() {
        return [ ];
    }
    public function search() {
        switch ($this->searchType) {
            case IndexForm::MOVIE :
                return $this->searchMovie ( $this->searchText );
                break;
            case IndexForm::ACTOR :
                return $this->searchActor ( $this->searchText );
                break;
        }
        
        return [ 
                'error' 
        ];
    }
    
    /**
     * 
     * @param string $searchText
     * @return string[]
     */
    protected function searchActor($searchText) {
        $roles = Role::find ()->select ( 'actor_id' )->orFilterWhere ( [ 
                'like',
                'name',
                $searchText 
        ] )->all ();
        
        $actor_ids = array();
        foreach ($roles as $role){
            $actor_ids[] = $role->actor_id;
        }
        
        $query = Actor::find ();
        
        $query->orFilterWhere ( [ 
                'like',
                'name',
                $searchText 
        ] );
        
        if(!empty($actor_ids)){
            $query->orFilterWhere ( [ 
                    'in',
                    'id',
                    $actor_ids
            ] );
        }
        
        $ids = [];
        
        foreach ($query->all() as $actor){
            $ids[] = $actor->id;

        }
        
        return [ 
                'actors',
                'ids' => $ids
        ];
    }
    
    /**
     * 
     * @param string $searchText
     * @return string[]
     */
    protected function searchMovie($searchText) {
        $query = Movie::find ();
        
        $query->orFilterWhere ( [ 
                'like',
                'title',
                $searchText 
        ] )->orFilterWhere ( [ 
                'like',
                'description',
                $searchText 
        ] );
        
        $ids = [];
        
        foreach ($query->all() as $movie){
            $ids[] = $movie->id;
        }
        
        return [ 
                'movies',
                'ids' => $ids
        ];
    }
}
