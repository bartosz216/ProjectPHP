<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * MovieGenre model
 *
 * @property integer $id
 * @property integer $genre_id
 * @property integer $movie_id
 */
class MovieGenre extends ActiveRecord {
	
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%movie_genre}}';
	}
	
	/**
	 * Finds MovieGenre by id
	 *
	 * @param string $id
	 * @return static|null
	 */
	public static function findById($id) {
		return static::findOne ( [ 'id' => $id ] );
	}
	
	/**
	 * Finds MovieGenre by $genre_id
	 *
	 * @param string $genre_id
	 * @return static|null
	 */
	public static function findByActoreId($genre_id){
		return static::findAll( ['genre_id' => $genre_id] );
	}
	
	/**
	 * Finds MovieGenre by $movie_id
	 *
	 * @param string $movie_id
	 * @return static|null
	 */
	public static function findByMovieId($movie_id){
		return static::findAll( ['movie_id' => $movie_id] );
	}
	
}