<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property integer $status
 * @property tinyint $type
 */
class User extends ActiveRecord implements IdentityInterface {
	const STATUS_UNSANCTIONED = 0;
	const STATUS_ACTIVE = 1;
	const STATUS_BANNED = 2;
	const STATUS_DELETED = 3;
	
	const TYPE_STANDARD = 0;
	const TYPE_ADMIN = 1;
	
	public function getComments()
	{
		return $this->hasMany(Comment::className(), ['user_id' => 'id']);
	}
	
	public function getUnsanctionedUser()
	{
		return $this->hasOne(UnsanctionedUser::className(), ['user_id' => 'id']);
	}
	
	public function getMovies(){
	    return $this->hasMany(Movie::className(), ['user_id' => 'id']);
	}
	
	public function getMarks(){
	    return $this->hasMany(Mark::className(), ['user_id' => 'id']);
	}
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						'status',
						'default',
						'value' => self::STATUS_UNSANCTIONED 
				],
				[ 
						'status',
						'in',
						'range' => [ 
								self::STATUS_UNSANCTIONED,
								self::STATUS_ACTIVE,
								self::STATUS_BANNED,
								self::STATUS_DELETED 
						] 
				],
				[ 
						'type',
						'default',
						'value' => self::TYPE_STANDARD
				],
				[ 
						'type',
						'in',
						'range' => [ 
								self::TYPE_STANDARD,
								self::TYPE_ADMIN
						] 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%user}}';
	}
	
	/**
	 * @inheritdoc
	 */
	public static function findIdentity($id) {
		return static::findOne ( [ 
				'id' => $id,
				'status' => self::STATUS_ACTIVE 
		] );
	}
	
	/**
	 * 
	 * @param integer $id
	 * @return static|null
	 */
	public static function findIdentityWithoutStatus($id)
	{
		return static::findOne ( [ 'id' => $id ] );
	}
	
	/**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken($token, $type = null) {
		throw new NotSupportedException ( '"findIdentityByAccessToken" is not implemented.' );
	}
	
	/**
	 * Finds user by username
	 *
	 * @param string $username        	
	 * @return static|null
	 */
	public static function findByUsername($username) {
		return static::findOne ( [ 
				'username' => $username,
				'status' => self::STATUS_ACTIVE 
		] );
	}
	
	/**
	 * @inheritdoc
	 */
	public function getId() {
		return $this->getPrimaryKey ();
	}
	
	/**
	 * @inheritdoc
	 */
	public function getAuthKey() {
		return null;
	}
	
	/**
	 * @inheritdoc
	 */
	public function validateAuthKey($authKey) {
		return true;
	}
	
	/**
	 * Validates password
	 *
	 * @param string $password
	 *        	password to validate
	 * @return bool if password provided is valid for current user
	 */
	public function validatePassword($password) {
		return $this->password === $password;
	}
}
