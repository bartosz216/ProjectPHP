<?php

namespace app\models;

use yii\db\ActiveRecord;
use app\models\User;

/**
 * UnsanctionedUser model
 *
 * @property integer $id
 * @property integer $user_id
 */
class UnsanctionedUser extends ActiveRecord {
    
	public function getUser()
	{
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

    /**
     * @inheritdoc
     */
    public function rules() {
        return [];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%unsanctioned_user}}';
    }
    
    /**
     * 
     */
    public static function findAllUnsactionedUsers()
    {
    	$users = [];
    	foreach(static::find()->all() as $user)
    	{
    		$user2 = User::findIdentityWithoutStatus($user->user_id);
    		if( $user2 !== null)
    		{
    			$users[$user2->id] = $user2->username;
    		}
    	}
    	
    	return $users;
    }
    
    /**
     * 
     */
    public static function findIdentity($id) {
        return static::findOne ( [ 'id' => $id  ] );
    }
}
