<?php

namespace app\models;

use yii\base\Model;

/**
 * MovieForm is the model behind the contact form.
 */
class ActorForm extends Model {
	public $actor;
	/**
	 *
	 * @return array the validation rules.
	 */
	public function rules() {
		return [];
	}
	
	/**
	 *
	 * @return array customized attribute labels
	 */
	public function attributeLabels() {
		return [];
	}
	
	public function remove(){
	    $actor_to_remove = new ActorToRemove ();
	    $actor_to_remove->actor_id = $this->actor->id;
	    $actor_to_remove->save();
	    
	    return $actor_to_remove;
	}
	
	public function edit(){
		\Yii::trace('edytuje aktora');
	}
}
