<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Role model
 *
 * @property integer $id
 * @property integer $actor_id
 * @property integer $movie_id
 * @property string $name
 */
class Role extends ActiveRecord {
	
	public function getActor()
	{
		return $this->hasOne(Actor::className(), ['id' => 'actor_id']);
	}
	
	public function getMovie()
	{
		return $this->hasOne(Movie::className(), ['id' => 'movie_id']);
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%role}}';
	}
	
	/**
	 * Finds role by id
	 *
	 * @param string $id
	 * @return static|null
	 */
	public static function findById($id) {
		return static::findOne ( [ 'id' => $id ] );
	}
	
	/**
	 * Finds role by $movie_id
	 *
	 * @param string $movie_id
	 * @return static|null
	 */
	public static function findByMovieId($movie_id){
		return static::findAll( ['movie_id' => $movie_id] );
	}
	
	/**
	 * Finds role by $actor_id
	 *
	 * @param string $actor_id
	 * @return static|null
	 */
	public static function findByActoreId($actor_id){
		return static::findAll( ['actor_id' => $actor_id] );
	}
	
	/**
	 * Finds role by $name
	 *
	 * @param string $name
	 * @return static|null
	 */
	public static function findByName($name){
		return static::findAll( ['name' => $name] );
	}
}