<?php

namespace app\models;

use Yii;
use yii\web\User;
use app\models\User as UserModel;

class ExtendedUser extends User {
    // Store model to not repeat query.
    private $_model;
    
    // This is a function that checks the field 'role'
    // in the User model to be equal to 1, that means it's admin
    // access it by Yii::app()->user->isAdmin()
    function isAdmin() {
        if ($this->isGuest) {
            return false;
        }
        
        $user = $this->loadUser ( Yii::$app->user->id );
        return intval ( $user->type ) == UserModel::TYPE_ADMIN;
    }
    
    // Load user model.
    protected function loadUser($id = null) {
        if ($this->_model === null) {
            if ($id !== null)
                $this->_model = UserModel::findIdentity ( $id );
        }
        return $this->_model;
    }
}