<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Actor model
 *
 * @property integer $id
 * @property string $name
 * @property date $date_of_birth
 * @property date $date_of_death
 */
class Actor extends ActiveRecord {
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getMoviesA() {
		return $this->hasMany ( Movie::className (), [ 
				'id' => 'movie_id' 
		] )->viaTable ( 'role', [ 
				'actor_id' => 'id' 
		], null, [ 
				'name' 
		] );
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getMoviesD() {
		return $this->hasMany ( Movie::className (), [ 
				'id' => 'movie_id' 
		] )->viaTable ( 'director', [ 
				'actor_id' => 'id' 
		] );
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getActorToRemove() {
		return $this->hasOne ( ActorToRemove::className (), [ 
				'actor_id' => 'id' 
		] );
	}
	
	/**
	 * 
	 * @return \yii\db\ActiveQuery
	 */
	public function getActorToEdit(){
		return $this->hasOne ( ActorToEdit::className (), [
				'actor_id' => 'id'
		] );
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%actor}}';
	}
	
	/**
	 * Finds actor by id
	 *
	 * @param string $id        	
	 * @return static|null
	 */
	public static function findById($id) {
		return static::findOne ( [ 
				'id' => $id 
		] );
	}
	
	/**
	 * Finds actor by name
	 *
	 * @param string $name        	
	 * @return static|null
	 */
	public static function findByName($name) {
		return static::findOne ( [ 
				'name' => $name 
		] );
	}
}
