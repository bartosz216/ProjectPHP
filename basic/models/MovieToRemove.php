<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Movie to remove model
 *
 * @property integer $id
 * @property integer $movie_id
 */
class MovieToRemove extends ActiveRecord {
    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMovie() {
        return $this->hasOne ( Movie::className (), [
                'id' => 'movie_id'
        ] );
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%movie_to_remove}}';
    }
    
    /**
     * Finds actor to remove by id
     *
     * @param string $id
     * @return static|null
     */
    public static function findById($id) {
        return static::findOne ( [
                'id' => $id
        ] );
    }
    
    /**
     * Finds actor to remove by movie_id
     *
     * @param integer $movie_id
     * @return static|null
     */
    public static function findByMovieId($movie_id) {
        return static::findOne ( [
                'movie_id' => $movie_id
        ] );
    }
}
