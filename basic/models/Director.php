<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Director model
 *
 * @property integer $id
 * @property integer $actor_id
 * @property integer $movie_id
 */
class Director extends ActiveRecord {
	
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%director}}';
	}
	
	/**
	 * Finds director by id
	 *
	 * @param string $id
	 * @return static|null
	 */
	public static function findById($id) {
		return static::findOne ( [ 'id' => $id ] );
	}
	
	/**
	 * Finds director by $movie_id
	 *
	 * @param string $movie_id
	 * @return static|null
	 */
	public static function findByMovieId($movie_id){
		return static::findAll( ['movie_id' => $movie_id] );
	}
	
	/**
	 * Finds director by $actor_id
	 *
	 * @param string $actor_id
	 * @return static|null
	 */
	public static function findByActoreId($actor_id){
		return static::findAll( ['actor_id' => $actor_id] );
	}
}