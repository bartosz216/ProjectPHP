<?php

namespace app\models;

use yii\base\Model;
use phpDocumentor\Reflection\Types\This;

/**
 * MovieForm is the model behind the contact form.
 */
class MovieForm extends Model {
    public $movie;
    public $comment;
    public $rate;
    /**
     *
     * @return array the validation rules.
     */
    public function rules() {
        return [ 
                [ 
                        'comment',
                        'validateComment' 
                ],
                [
                        [
                                'rate'
                        ],
                        'integer'
                ]
        ];
    }
    
    /**
     *
     * @return array customized attribute labels
     */
    public function attributeLabels() {
        return [ 
                'comment' => 'Dodaj komentarz:',
                'rate' => 'Ocena:',
        ];
    }
    
    public function init(){
    	$this->movie = Movie::findById ( \Yii::$app->request->get ( 'id' ) );
    }
    
    public function rates(){
        $rates = [];
        for ($i=0; $i <= 10; $i++){
            $rates[] = $i;
        }
        return $rates;
    }
    
    /**
     *
     * @return NULL|\app\models\Comment
     */
    public function addComment() {
        if (! $this->validate ()) {
            return null;
        }
        
        $comment = new Comment ();
        $comment->movie_id = $this->movie->id;
        $comment->user_id = \Yii::$app->user->id;
        $comment->text = $this->comment;
        $comment->save ();
        
        return $comment;
    }
    
    /**
     * 
     * @return \app\models\Mark
     */
     public function addMark(){
        $mark = new Mark();
        $mark->user_id = \Yii::$app->user->id;
        $mark->movie_id = $this->movie->id;
        $mark->mark = $this->rate;
        $mark->save();
        
        return $mark;
    }
    
    public function remove(){
        $movie_to_remove = new MovieToRemove();
        $movie_to_remove->movie_id = $this->movie->id;
        $movie_to_remove->save();
        
        return $movie_to_remove;
    }
    
    /**
     *
     * @param string $attribute            
     * @param array $params            
     */
    public function validateComment($attribute, $params) {
        if (! $this->hasErrors ()) {
            $array = Comment::findByMovieAdnUserId ( $this->movie->id, \Yii::$app->user->id );
            if (! empty ( $array )) {
                $this->addError ( $attribute, "Nie możesz dodać więc komentarzy pod tym filmem." );
            }
        }
    }
}
