<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Movie model
 *
 * @property integer $id
 * @property integer $movie_id
 * @property integer $user_id
 * @property integer $mark
 */
class Mark extends ActiveRecord {
    public function getUser() {
        return $this->hasOne ( User::className (), [ 
                'id' => 'user_id' 
        ] );
    }
    public function getMovie() {
        return $this->hasOne ( Movie::className (), [ 
                'id' => 'movie_id' 
        ] );
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%mark}}';
    }
    
    /**
     * Finds mark by id
     *
     * @param string $id            
     * @return static|null
     */
    public static function findById($id) {
        return static::findOne ( [ 
                'id' => $id 
        ] );
    }
    
    /**
     * Finds mark by user id
     *
     * @param string $user_id            
     * @return static|null
     */
    public static function findByUserId($user_id) {
        return static::findAll ( [ 
                'user_id' => $user_id 
        ] );
    }
    
    /**
     * Finds mark by movie id
     *
     * @param string $movie_id            
     * @return static|null
     */
    public static function findByMovieId($movie_id) {
        return static::findAll ( [ 
                'movie_id' => $movie_id 
        ] );
    }
    
    /**
     * Finds mark by movie id and user id
     *
     * @param string $movie_id
     * @param string $user_id
     * @return static|null
     */
    public static function findByMovieIdAndUserId($movie_id, $user_id) {
        return static::findAll ( [
                'movie_id' => $movie_id,
                'user_id' => $user_id
        ] );
    }
}
