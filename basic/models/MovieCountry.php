<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * MovieCountry model
 *
 * @property integer $id
 * @property integer $country_id
 * @property integer $movie_id
 */
class MovieCountry extends ActiveRecord {
	
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%movie_country}}';
	}
	
	/**
	 * Finds MovieCountry by id
	 *
	 * @param string $id
	 * @return static|null
	 */
	public static function findById($id) {
		return static::findOne ( [ 'id' => $id ] );
	}
	
	/**
	 * Finds MovieCountry by $country_id
	 *
	 * @param string $country_id
	 * @return static|null
	 */
	public static function findByActoreId($country_id){
		return static::findAll( ['country_id' => $country_id] );
	}
	
	/**
	 * Finds MovieCountry by $movie_id
	 *
	 * @param string $movie_id
	 * @return static|null
	 */
	public static function findByMovieId($movie_id){
		return static::findAll( ['movie_id' => $movie_id] );
	}

}