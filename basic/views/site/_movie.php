<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
use app\models\Country;
use app\models\MovieCountry;
use app\models\MovieGenre;
use app\models\Genre;
use app\models\Mark;
?>
<style>
.movie
{
    text-align: left;
    margin-bottom: 20px;
}

.box1{
    width:auto;
    height:auto;
    display:inline-block;
    margin-right: 20px;
}
.box2{
    height:auto;
    display:inline-block;
}

</style>

<div class="movie">
		<div class="box1">
			<?= Html::img($model->poster, ['width' => '122', 'height' => '182'])?>
		</div>
		<div class="box2">
		    <?= Html::a(Html::encode($model->title), Url::to(['site/movie', 'id' => $model->id])) ?> <br>
	    	<?= HtmlPurifier::process($model->description) ?>  <br>
	    	<?php
	    		echo 'Kraj produkcji: ';
	    		foreach(MovieCountry::findByMovieId($model->id) as $movieCountry){
	    			$country = Country::findById($movieCountry->country_id);
	    			echo $country->name;
	    		}
	    		echo '<br>';
	    		echo 'Rodzaj filmu: ';
	    		foreach(MovieGenre::findByMovieId($model->id) as $movieGenre){
	    			$genre = Genre::findById($movieGenre->genre_id);
	    			echo $genre->name;
					echo ' ';
	    		}
	    		$mark = Mark::find()->select(['AVG(mark) as mark'])->where(['movie_id' => $model->id])->one();
	    		//Yii::trace($mark->mark);
	    		echo '<br>Ocena: ';
	    		if($mark->mark == null){
	    		    echo 'Brak ocen.';
	    		}else{
	    		    echo $mark->mark.' / 10';
	    		}
	    	?>
		</div>
</div>