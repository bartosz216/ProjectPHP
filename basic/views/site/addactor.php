<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\AddActorForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Dodaj aktora/reżysera';
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="site-addactor">
	<h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(['id' => 'addactor-form']); ?>

        <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

        <?=$form->field ( $model, 'date_of_birth' )->widget ( \yii\widgets\MaskedInput::className (), [ 'mask' => '9999-99-99' ] )?>

         <?=$form->field ( $model, 'date_of_death' )->widget ( \yii\widgets\MaskedInput::className (), [ 'mask' => '9999-99-99' ] )?>
        
        <div class="form-group">
        	<?= Html::submitButton('Dodaj', ['class' => 'btn btn-primary', 'name' => 'addactor-button']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>
    <p>work in progess.</p>
</div>