<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>
<div class="post">
    <h2><?= Html::encode($model->title) ?></h2>
	<?= Html::img($model->poster)?>
    <?= HtmlPurifier::process($model->description) ?>  
    <?= Html::label($model->budget) ?>  
</div>