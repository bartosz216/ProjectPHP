<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<style>
.actor
{
    text-align: left;
    margin-bottom: 20px;
}
</style>

<div class="actor">
		<div class="box2">
		    <?= Html::a(Html::encode($model->name), Url::to(['site/actor', 'id' => $model->id])) ?> <br>
		    Data urodzenia: <?= $model->date_of_birth ?> <br>
		    Data śmierci: 
		    <?php if($model->date_of_death !== null){
		    	echo $model->date_of_death;
		    }else{
		    	echo '-';
		    }?>
		</div>

</div>