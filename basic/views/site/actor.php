<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\models\Role;
use app\models\Movie;
use app\models\Director;
use app\models\ActorToRemove;

$this->title = $model->actor->name;
$this->params ['breadcrumbs'] [] = $this->title;
?>
<style>
td{
	padding-right: 20px;
	padding-left: 20px;
	width: 250px;
}
.parent
{
    text-align: left;
    margin-bottom: 20px;
    overflow:auto; 
}

.box1{
    float:left;
    margin-right: 20px;
}

.box2{
    display: table-cell;
    float:right; 
}

button{
	width: 100%;
}
</style>
<div class="site-actor">
	<div class="parent">
		<div class="box1">
			<h1><?= Html::encode($this->title) ?></h1>
		    Data urodzenia: <?= $model->actor->date_of_birth ?> <br>
		    Data śmierci: 
		    <?php if($model->actor->date_of_death !== null){
		    	echo $model->actor->date_of_death;
		    }else{
		    	echo '-';
		    }?>
    	</div>
    	<div class="box2">
   			<?php $form = ActiveForm::begin(['id' => 'actor-form']); ?>
   			<div class="form-group">
			    <?php
			         $to_remove = ActorToRemove::findByActorId($model->actor->id);
			         if(empty($to_remove)){
			             echo Html::submitButton('Usuń', ['class' => 'btn btn-primary', 'name' => 'delete-button']).'<br>';
			         }
			         echo Html::submitButton('Edytuj', ['class' => 'btn btn-primary', 'name' => 'edit-button']);
			    ?>
			</div>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
	<h3>Filmografia:</h3>
	<h4>Reżyser:</h4>
	<?php 
		foreach(Director::findByActoreId($model->actor->id) as $director){
			$movie = Movie::findById($director->movie_id);
			echo Html::a(Html::encode($movie->title), Url::to(['site/movie', 'id' => $movie->id])).'<br>';
		}
	?>
	
	<h4>Aktor:</h4>
	<?php 
		echo '<table>';
		foreach(Role::findByActoreId($model->actor->id) as $director){
			$movie = Movie::findById($director->movie_id);
			echo '<tr><td>'.Html::a(Html::encode($movie->title), Url::to(['site/movie', 'id' => $movie->id])).'</td><td>'.$director->name.'</td></tr>';

		}
		echo '</table>';
	?>

</div>