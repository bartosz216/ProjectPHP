<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\IndexForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'My Yii Application';
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="site-index">
    <?php $form = ActiveForm::begin(['id' => 'index-form', 'layout' => 'horizontal']); $model->searchType = 0;?>

        <?= $form->field($model, 'searchText', [
                'template' => '<div class="input-group">{input}<span class="input-group-btn">' .
                Html::submitButton('Szukaj', ['class' => 'btn btn-primary']) .'</span></div>'
            ])->textInput(['autofocus' => true]) ?>
        <?= $form->field($model, 'searchType')->inline(true)->radioList(['Film', 'Aktor']) ?>
        

    <?php ActiveForm::end(); ?>
</div>
