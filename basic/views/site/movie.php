<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\models\MovieCountry;
use app\models\Country;
use app\models\MovieGenre;
use app\models\Genre;
use app\models\Director;
use app\models\Actor;
use app\models\Role;
use app\models\User;
use app\models\Comment;
use app\models\Mark;
use app\models\MovieToRemove;

$this->title = $model->movie->title;
$this->params ['breadcrumbs'] [] = $this->title;
?>
<style>
td{
	padding-right: 20px;
	padding-left: 20px;
	width: 250px;
}
.parent
{
    text-align: left;
    margin-bottom: 20px;
    overflow:auto; 
}

.box1{
    float:left;
    margin-right: 20px;
}

.box2{
    float:left;
    margin-right: 20px;
}
.box3{
    display: table-cell;
    float:right;
}
button{
    width: 100%
}
</style>
<div class="site-movie">
	<div class="parent">
		<div class="box1">
			<?= Html::img($model->movie->poster, ['width' => '183', 'height' => '273'])?>
		</div>
		<div class="box2">
		<h1><?= Html::encode($this->title) ?></h1>
		<?= HtmlPurifier::process($model->movie->description) ?>  <br>
		<?php
		echo 'W reĹĽyserii: ';
		foreach (Director::findByMovieId($model->movie->id) as $director ){
			$actor = Actor::findById($director->actor_id);
			echo Html::a(Html::encode($actor->name), Url::to(['site/actor', 'id' => $actor->id]));
			echo ', ';
		}
	 	
		echo '<br> Kraj produkcji: ';
		foreach ( MovieCountry::findByMovieId ( $model->movie->id ) as $movieCountry ) {
			$country = Country::findById ( $movieCountry->country_id );
			echo $country->name;
			echo ', ';
		}
		echo '<br>';
		echo 'Rodzaj filmu: ';
		foreach ( MovieGenre::findByMovieId ( $model->movie->id ) as $movieGenre ) {
			$genre = Genre::findById ( $movieGenre->genre_id );
			echo $genre->name;
			echo ', ';
		}
		echo '<br> Rok produkcji ';
		echo $model->movie->year;
		$user = User::findIdentityWithoutStatus($model->movie->user_id);
		echo '<br> Dodane przez: ';
		echo $user->username;
		?> <br>
		BudĹĽet: <?=$model->movie->budget ?> mln <br>
		Ocena: 
		<?php 
		  $mark = Mark::find()->select(['AVG(mark) as mark'])->where(['movie_id' => $model->movie->id])->one();
    		if($mark->mark == null){
    		    echo 'Brak ocen.';
    		}else{
    		    echo $mark->mark.' / 10';
    		}
		?>
		</div>
		<div class="box3">
			<?php $form = ActiveForm::begin(['id' => 'action-form']);
			     $rate = Mark::findByMovieIdAndUserId($model->movie->id, Yii::$app->user->id);
			     if(empty($rate)){
    		         echo $form->field($model, 'rate')->dropDownList($model->rates());
    				 echo Html::submitButton('Oceń', ['class' => 'btn btn-primary', 'name' => 'rate-button']).'<br>';
			     }
			     $to_remove = MovieToRemove::findByMovieId($model->movie->id);
			     if(empty($to_remove)){
			         echo Html::submitButton('Usuń', ['class' => 'btn btn-primary', 'name' => 'delete-button']).'<br>';
			     }
			     echo Html::submitButton('Edytuj', ['class' => 'btn btn-primary', 'name' => 'edit-button']);
			     ActiveForm::end();
	       ?>
		</div>
	</div>
	<h3>Obsada:</h3>
	<?php 
		echo '<table>';
		foreach(Role::findByMovieId($model->movie->id) as $role){
			$actor = Actor::findById($role->actor_id);
			echo '<tr><td>'.Html::a(Html::encode($actor->name), Url::to(['site/actor', 'id' => $actor->id])).'</td><td>'.$role->name.'</td></tr>';

		}
		echo '</table>';
	?>
	
	<h2>Komentarze:</h2>
	<?php 
	if(!Yii::$app->user->isGuest){
	    $array = Comment::findByMovieAdnUserId($model->movie->id, Yii::$app->user->id);
	    if(empty( $array ))
	    {
        	$form = ActiveForm::begin(['id' => 'movie-form']);
            echo $form->field($model, 'comment')->textarea(['rows' => 6]);
            echo Html::submitButton('Dodaj', ['class' => 'btn btn-primary', 'name' => 'comment-button']);
        	ActiveForm::end();
	    }
	}
	
    foreach(Comment::findByMovieId($model->movie->id) as $comment){
        $user = User::findIdentity($comment->user_id);
        echo $comment->date.' '.$user->username.'<br>'.$comment->text.'<br>';
    }
    ?>

</div>