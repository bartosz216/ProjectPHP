<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = $model->name;
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="site-editactor">
	<h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(['id' => 'editactor-form']); ?>

        <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

        <?=$form->field ( $model, 'date_of_birth' )->widget ( \yii\widgets\MaskedInput::className (), [ 'mask' => '9999-99-99' ] )?>

         <?=$form->field ( $model, 'date_of_death' )->widget ( \yii\widgets\MaskedInput::className (), [ 'mask' => '9999-99-99' ] )?>
        
        <div class="form-group">
        	<?= Html::submitButton('Zgłoś zmiany', ['class' => 'btn btn-primary', 'name' => 'editactor-button']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>
    <p>work in progess.</p>
</div>