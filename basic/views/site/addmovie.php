<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\AddMovieForm */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Dodaj film';
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="site-addmovie">
	<h1><?= Html::encode($this->title) ?></h1>

           	<?php $form = ActiveForm::begin(['id' => 'addmovie-form']); ?>

			<?= $form->field($model, 'title')->textInput(['autofocus' => true]) ?>
			
			<?= $form->field($model, 'directors')->textarea()?>
			
			<?= $form->field($model, 'actors')->textarea()?>
			
			<?= $form->field($model, 'genres')->textInput() ?>
			
			<?= $form->field($model, 'countries')->textInput() ?>
			
			<?= $form->field($model, 'budget')->textInput() ?>
			
			<?= $form->field($model, 'year')->textInput() ?>

			<?= $form->field($model, 'poster')->textInput() ?>
			
			<?= $form->field($model, 'description')->textarea()?>
			
			<div class="form-group">
        		<?= Html::submitButton('Dodaj', ['class' => 'btn btn-primary', 'name' => 'addmovie-button']) ?>
        	</div>

           	<?php ActiveForm::end(); ?>

    <p>work in progess.</p>
</div>