<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\AdminPanelForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Panel administracyjny';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-adminpanel">
    <h1><?= Html::encode($this->title) ?></h1>
	<div class="row">
        <div class="col-lg-5">

            <?php $form = ActiveForm::begin(['id' => 'adminpanel-form']); ?>

                <?= $form->field($model, 'reportType')->dropDownList($model->getReportTypes(), ['autofocus' => true, 'onchange' => '']); ?>
                
                <?= $form->field($model, 'reports')->listBox($model->getReport(), ['multiple' => true]); ?>
                
             	<div class="form-group">
            		<?= Html::submitButton('Zaakceptuj', ['class' => 'btn btn-primary', 'name' => 'accept-button']) ?>
    			</div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>