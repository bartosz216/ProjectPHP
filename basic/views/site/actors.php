<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\base\Widget;

$this->title = 'Aktorzy i reżyserzy';
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="site-actors">
	<h1><?= Html::encode($this->title) ?></h1>
	<?=ListView::widget ( [ 
			'dataProvider' => $dataProvider,
			'itemView' => '_actor',
			'pager' => [
					'firstPageLabel' => 'first',
					'lastPageLabel' => 'last',
					'prevPageLabel' => 'previous',
					'nextPageLabel' => 'next',
					'maxButtonCount' => 3,
				]
	] )?>
</div>